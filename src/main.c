// Rachel Boylan
// BLE-Lab
// December 15, 2023

// Including to tell it to look at these files as well
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>
#include <zephyr/drivers/adc.h> 
#include <zephyr/drivers/pwm.h>
#include "pressure.h" // pressure sensor
#include <zephyr/drivers/sensor.h> // pressure sensor?
#include <nrfx_power.h>
#include <math.h>

// Bluetooth include statments
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>

LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

#define HEARTBEAT_INTERVAL 1000
#define LED_INTERVAL 1000
#define ERROR_INTERVAL 1000
#define MEASUREMENT_DELAY_MS 1000
#define ARR_SIZE 100

int16_t buf;
int16_t buf1 = 0;
int16_t buf2 = 0;
int32_t val_mv = 0;
int32_t val_mv1 = 0;
int32_t val_mv2;
float LED1_bright;
float LED2_bright;
int32_t battery; 
static int32_t atm_pressure_kPa;
int i;
int j;

float avg_max = 0.0;
float avg_min = 0.0;
float avg_max2 = 0.0;
float avg_min2 = 0.0;

int i2;
float data_arr[ARR_SIZE] = {};
float data_arr2[ARR_SIZE] = {};

int max_val[10] = {};
int min_val[10] = {};
int max_val2[10] = {};
int min_val2[10] = {};

int oversample = 10; 
int read_pressure_sensor(const struct device *pressure_in, int oversample, int atm_offset_daPa);
int64_t usbregstatus;


void read(void);

struct adc_sequence sequence = {
	.buffer = &buf,
	.buffer_size = sizeof(buf), // bytes
};
struct adc_sequence sequence1 = {
	.buffer = &buf1,
	.buffer_size = sizeof(buf1), // bytes
};
struct adc_sequence sequence2 = {
	.buffer = &buf2,
	.buffer_size = sizeof(buf2), // bytes
};

/* Define some macros to use some Zephyr macros to help read the DT
configuration based on the ADC channel alias (I have no idea why this
macro is not available in adc.h) */
#define ADC_DT_SPEC_GET_BY_ALIAS(vadc)                    \
{                                                            \
    .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(vadc))),      \
    .channel_id = DT_REG_ADDR(DT_ALIAS(vadc)),            \
    ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(vadc))          \
}                                                            \

// configing button 2 
#define BT_UUID_REMOTE_SERV_VAL \
        BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_SERVICE 			BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

/* UUID of the Data Characteristic */
#define BT_UUID_REMOTE_DATA_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_DATA_CHRC 		BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

/* UUID of the Message Characteristic */
#define BT_UUID_REMOTE_MESSAGE_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0002, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_MESSAGE_CHRC 	BT_UUID_DECLARE_128(BT_UUID_REMOTE_MESSAGE_CHRC_VAL) 

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
#define BLE_DATA_POINTS 600 // limited by MTU

int battery_level;
int normalized_battery_level;

enum bluetooth_outputs {led1_brightness, led2_brightness, pressure_reading};
int16_t bluetooth_output[3];

enum bt_data_notifications_enabled {
    BT_DATA_NOTIFICATIONS_ENABLED,
    BT_DATA_NOTIFICATIONS_DISABLED,
};
enum bt_data_notifications_enabled notifications_enabled;

// declare struct of the BLE remove service callbacks
struct bt_remote_srv_cb {
    void (*notif_changed)(enum bt_data_notifications_enabled status);
    void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
}; 


struct bt_remote_srv_cb remote_service_callbacks;

// blocking thread semaphore to wait for BLE to initialize
static K_SEM_DEFINE(bt_init_ok, 1, 1);

/* Advertising data */
static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
};

/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
};

// Intialize the ADC/PWM struct to store all the DT parameters
static const struct adc_dt_spec adc_vadc = ADC_DT_SPEC_GET_BY_ALIAS(vadc);
static const struct adc_dt_spec adc_vadc1 = ADC_DT_SPEC_GET_BY_ALIAS(vadc1);
//static const struct adc_dt_spec adc_vadc2 = ADC_DT_SPEC_GET_BY_ALIAS(vadc2);
static const struct pwm_dt_spec led_pwm = PWM_DT_SPEC_GET(DT_ALIAS(pwm1));
static const struct pwm_dt_spec led_pwm2 = PWM_DT_SPEC_GET(DT_ALIAS(pwm2));
const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr); 

// Heartbeat struct handlers/work handlers
void heartbeat_timer_handler(struct k_timer *heartbeat_timer);
void heartbeat_timer_work_handler(struct k_work *hb_timer_work);

// LEDs timer handlers/work handlers
void led_timer_start_handler(struct k_timer *led_timer);
void led_timer_start_work_handler(struct k_work *led_timer_start_work);
void led_timer_stop_handler(struct k_timer *led_timer);
void led_timer_stop_work_handler(struct k_work *led_timer_stop_work);

// Error led timer
void error_led_timer_handler(struct k_timer *error_led_timer);
void error_led_timer_work_handler(struct k_work *error_led_timer_work);

// Button1 handlers/work handlers
void button1_on_cb_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void button1_on_cb_work_handler(struct k_work *button1_on_cb_work);

// Button2 handlers/work handlers
void button2_on_cb_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void button2_on_cb_work_handler(struct k_work *button2_on_cb_work);

// Heartbeat k TIMER and WORK
K_TIMER_DEFINE (heartbeat_timer, heartbeat_timer_handler, NULL);
K_WORK_DEFINE (hb_timer_work, heartbeat_timer_work_handler);

// LED TIMER and WORK
K_TIMER_DEFINE (led_timer, led_timer_start_handler, led_timer_stop_handler);
K_WORK_DEFINE (led_timer_start_work, led_timer_start_work_handler);
K_WORK_DEFINE (led_timer_stop_work, led_timer_stop_work_handler);

// ERROR LED TIMER and WORK
K_TIMER_DEFINE (error_led_timer, error_led_timer_handler, NULL);
K_WORK_DEFINE (error_led_timer_work, error_led_timer_work_handler);

// Button 1 TIMER and WORK
K_WORK_DEFINE (button1_on_cb_work, button1_on_cb_work_handler); 

// Button 2 TIMER and WORK
K_WORK_DEFINE (button2_on_cb_work, button2_on_cb_work_handler); 

// Heartbeat/LED4 Alias Defined and Creating Strutures from the Alias
#define HEARTBEAT DT_ALIAS(heartbeat)
static const struct gpio_dt_spec heartbeat = GPIO_DT_SPEC_GET(HEARTBEAT, gpios);

// LED1 Alias Defined and Creating Strutures from the Alias
#define LED0 DT_ALIAS(led1)
static const struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET(LED0, gpios);

// LED2 Alias Defined and Creating Strutures from the Alias
#define LED1 DT_ALIAS(led2)
static const struct gpio_dt_spec led2 = GPIO_DT_SPEC_GET(LED1, gpios);

// LED3 Alias Defined and Creating Strutures from the Alias
#define LED2 DT_ALIAS(led3)
static const struct gpio_dt_spec led3 = GPIO_DT_SPEC_GET(LED2, gpios);

// BUTTON 1 Alias Defined and Creating Strutures from the Alias
#define BUTTON0 DT_ALIAS(button1)
static const struct gpio_dt_spec button1 = GPIO_DT_SPEC_GET(BUTTON0, gpios);

// BUTTON 2 Alias Defined and Creating Strutures from the Alias
#define BUTTON1 DT_ALIAS(button2)
static const struct gpio_dt_spec button2 = GPIO_DT_SPEC_GET(BUTTON1, gpios);

// Creating a Callback structure for both buttons
static struct gpio_callback button1_on_cb;
static struct gpio_callback button2_on_cb;

// Declaring states entry, run, and exits
static int init_entry(void *o);
static void init_run(void *o);
static void idle_entry(void *o);
static void idle_run(void *o);
static void idle_exit(void *o);
static void measure_entry(void *o);
static void measure_run(void *o);
static void measure_exit(void *o);
static void errorpressure_entry(void *o);
static void errorpressure_run(void *o);
static void errorpressure_exit(void *o);
static void errorvbus_entry(void *o);
static void errorvbus_run(void *o);
static void errorvbus_exit(void *o);

// State Machine Creation
static const struct smf_state my_states[];

// List of states
enum my_state { 
	init, idle, measure, errorpressure, errorvbus
};

// User defined object
struct s_object {
    struct smf_ctx ctx;
} 
s_obj;

static const struct smf_state my_states[] = {
   [init] = SMF_CREATE_STATE(init_entry, init_run, NULL),
   [idle] = SMF_CREATE_STATE(idle_entry, idle_run, idle_exit),
   [measure] = SMF_CREATE_STATE(measure_entry, measure_run, measure_exit),
   [errorpressure] = SMF_CREATE_STATE(errorpressure_entry, errorpressure_run, errorpressure_exit),
   [errorvbus] = SMF_CREATE_STATE(errorvbus_entry, errorvbus_run, errorvbus_exit),
};

int ret;

// State Init
static int init_entry(void *o)
{
	LOG_INF("in init entry");

	k_timer_start(&heartbeat_timer, K_MSEC(HEARTBEAT_INTERVAL), K_MSEC(HEARTBEAT_INTERVAL));
	 

    // Checking if device is ready!
	if (!device_is_ready(heartbeat.port)) {
		return ret;
	}

	// Check that the ADC interface is ready: CHANNEL 1
	if (!device_is_ready(adc_vadc.dev)) {
		LOG_ERR("ADC controller device(s) not ready");
		return -1;
	}

	// Check that the ADC interface is ready: CHANNEL 2
	if (!device_is_ready(adc_vadc1.dev)) {
		LOG_ERR("ADC1 controller device(s) not ready");
		return -1;
	}

	// Check that the ADC interface is ready: CHANNEL 3
	// if (!device_is_ready(adc_vadc2.dev)) {
	// 	LOG_ERR("ADC1 controller device(s) not ready");
	// 	return -1;
	// }

	// Check that the PWM channel is ready: CHANNEL 1
	if (!device_is_ready(led_pwm.dev))  {
		LOG_ERR("PWM device %s is not ready.", led_pwm.dev->name);
		return -1;
	}

	// Check that the PWM channel is ready: CHANNEL 2
	if (!device_is_ready(led_pwm2.dev))  {
		LOG_ERR("PWM device %s is not ready.", led_pwm2.dev->name);
		return -1;
	}

	// is pressure ready
	if (!device_is_ready(pressure_in)) {
		LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
		return;
	}
    else {
        LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
    }

	// Configure the ADC CHANNEL 1
	int err;
	err = adc_channel_setup_dt(&adc_vadc);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel (%d)", err);
		return err;
	}

	// Configure the ADC channel 2
	err = adc_channel_setup_dt(&adc_vadc1);
	if (err < 0) {
		LOG_ERR("Could not setup ADC1 channel (%d)", err);
		return err;
	}

	// Configure the ADC channel 3 
	// err = adc_channel_setup_dt(&adc_vadc2);
	// if (err < 0) {
	// 	LOG_ERR("Could not setup ADC2 channel (%d)", err);
	// 	return err;
	// }

	// Configure the PWM channel 1
	// err = pwm_set_pulse_dt(&led_pwm, led_pwm.period); // at full duty cycle
	// if (err) {
	// 	LOG_ERR("Could not set led pwm driver 1 (PWM0)");
	// }

	// // Configure the PWM channel 2
	// err = pwm_set_pulse_dt(&led_pwm2, led_pwm2.period); // at full duty cycle
	// if (err) {
	// 	LOG_ERR("Could not set led pwm driver 2 (PWM0)");
	// }
	
	ret = gpio_pin_configure_dt(&button1, GPIO_INPUT);
	if (ret < 0) {
		return;
		return ret;
	}

    ret = gpio_pin_configure_dt(&button2, GPIO_INPUT);
	if (ret < 0) {
		return;
		return ret;
	}

	// LEDs ACTIVE 
	ret = gpio_pin_configure_dt(&heartbeat, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

	ret = gpio_pin_configure_dt(&led1, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

	ret = gpio_pin_configure_dt(&led2, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

	ret = gpio_pin_configure_dt(&led3, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

	// float atm_pressure_kPa = read_pressure_sensor(pressure_in, oversample, 0);
	// k_msleep(MEASUREMENT_DELAY_MS);

	// if (atm_pressure_kPa <= 0) {
    //     smf_set_state(SMF_CTX(&s_obj), &my_states[errorpressure]);
    // } 
    

	// LOG_INF("Pressure value: %f", atm_pressure_kPa);

	//k_timer_start(&led_timer, K_MSEC(LED_INTERVAL), K_MSEC(LED_INTERVAL));
}

static void init_run(void *o){
	LOG_INF("in init run");
	smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
}

static void idle_entry(void *o){
	LOG_INF("in idle entry");
	int ret;

	//k_timer_stop(&led_timer); 

	// I commented this out dont forget
	gpio_pin_set_dt(&led3, 0);
	gpio_pin_set_dt(&led2, 0);
	gpio_pin_set_dt(&led1, 0);

    // configing button 1 
	ret = gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_EDGE_TO_ACTIVE);
	gpio_init_callback(&button1_on_cb, button1_on_cb_handler, BIT(button1.pin));	
	gpio_add_callback(button1.port, &button1_on_cb);

    // configing button 2 
    ret = gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_EDGE_TO_ACTIVE);
	gpio_init_callback(&button2_on_cb, button2_on_cb_handler, BIT(button2.pin));	
	gpio_add_callback(button2.port, &button2_on_cb);

	// float atm_pressure_kPa = read_pressure_sensor(pressure_in, oversample, 0);
	// k_msleep(MEASUREMENT_DELAY_MS);

	// if (atm_pressure_kPa <= 0) {
    //     smf_set_state(SMF_CTX(&s_obj), &my_states[errorpressure]);
    // } 
    
	// LOG_INF("Pressure value: %f", atm_pressure_kPa);

	// if (usbregstatus == 0){
    //     smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
    // }
	// if (usbregstatus == 1) {
	// 	smf_set_state(SMF_CTX(&s_obj), &my_states[errorvbus]);
	// }

}
static void idle_run(void *o){
    k_msleep(1000);
	LOG_INF("idle run");

	//UNNNNCOMMMENT THIS IS PRESSUREEEEE CHECKINGGG
	// float atm_pressure_kPa = read_pressure_sensor(pressure_in, oversample, 0);
	// k_msleep(MEASUREMENT_DELAY_MS);

	// usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
	// if (atm_pressure_kPa <= 0) {
    //     smf_set_state(SMF_CTX(&s_obj), &my_states[errorpressure]);
    // } 
    
	LOG_INF("Pressure value: %f", atm_pressure_kPa);

	// if (usbregstatus == 0){
    //     smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
    // }

	usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
	if (usbregstatus == 1) {
		smf_set_state(SMF_CTX(&s_obj), &my_states[errorvbus]);
	}
}

static void idle_exit(void *o){
	LOG_INF("idle exit");
}

static void measure_entry(void *o){
    LOG_INF("in measure entry");
	gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_DISABLE);
    gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_DISABLE);
	k_timer_start(&led_timer, K_MSEC(ARR_SIZE), K_MSEC(ARR_SIZE));
	//k_timer_start(&led2_timer, K_MSEC(ARR_SIZE), K_MSEC(ARR_SIZE));

}

static void measure_run(void *o){
  
	//for (int i = 0; i < ARR_SIZE; i++){


		 LOG_INF("Measuring %s (channel %d)... ", adc_vadc.dev->name, adc_vadc.channel_id);
		 LOG_INF("Measuring %s (channel %d)... ", adc_vadc1.dev->name, adc_vadc1.channel_id);

		(void)adc_sequence_init_dt(&adc_vadc, &sequence);
		(void)adc_sequence_init_dt(&adc_vadc1, &sequence1);
		// (void)adc_sequence_init_dt(&adc_vadc2, &sequence2);

		ret = adc_read(adc_vadc.dev, &sequence);
		if (ret < 0) {
			LOG_ERR("Could not read (%d)", ret);
		} else {
			LOG_DBG("Raw ADC Buffer: %d", buf);
		}

		int ret1;
		ret1 = adc_read(adc_vadc1.dev, &sequence1);
		if (ret1 < 0) {
			LOG_ERR("Could not read (%d)", ret1);
		} else {
			LOG_DBG("Raw ADC Buffer 1: %d", buf1);
		}
		
		val_mv = buf;
		val_mv1 = buf1;

		ret = adc_raw_to_millivolts_dt(&adc_vadc, &val_mv); // remember that the vadc struct containts all the DT parameters
		if (ret < 0) {
			LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
		} else {
			LOG_INF("ADC Value (mV): %d", val_mv);
		}
		ret1 = adc_raw_to_millivolts_dt(&adc_vadc1, &val_mv1); // remember that the vadc struct containts all the DT parameters
		if (ret1 < 0) {
			LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
		} else {
			LOG_INF("ADC Value 1 (mV): %d", val_mv1);
		}
 	//}
	LOG_INF("Finishing measurments");
    LOG_INF("in measure run");
	//smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
}

static void measure_exit(void *o){
    LOG_INF("in measure exit");
	
	smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
	//k_timer_stop(&led_timer);
	LOG_INF("Exited");
	//k_timer_stop(&led_timer);
	
}

static void errorpressure_entry(void *o){
    LOG_INF("in error pressure entry");

	//k_timer_stop(&led_timer);
	//k_timer_start(&error_led_timer, K_MSEC(ERROR_INTERVAL), K_MSEC(ERROR_INTERVAL));
	gpio_pin_set_dt(&led2, 0);
	gpio_pin_set_dt(&led1, 0);
	gpio_pin_set_dt(&led3, 1);

	gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_DISABLE);
    gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_DISABLE);

	//LOG_INF("ERROR ILLUMINATE");
}

static void errorpressure_run(void *o){
    LOG_INF("in error pressure run");
	LOG_INF("ERROR ILLUMINATE");
	
	float atm_pressure_kPa = read_pressure_sensor(pressure_in, oversample, 0);
	k_msleep(MEASUREMENT_DELAY_MS);
	if (atm_pressure_kPa >= 0) {
        smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
    } 
	//k_timer_start(&led_timer, K_MSEC(LED_INTERVAL),K_MSEC(LED_INTERVAL));
	//k_timer_stop(&error_led_timer);
	//k_msleep(100);
	//smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
}

static void errorpressure_exit(void *o){
    LOG_INF("in error pressure exit");

	LOG_INF("ERROR OFF");
	//k_timer_start(&led_timer, K_MSEC(LED_INTERVAL),K_MSEC(LED_INTERVAL));
	//k_timer_stop(&error_led_timer);
	//smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);

}

static void errorvbus_entry(void *o){
    LOG_INF("in error vbus entry");

    //k_timer_stop(&led_timer);
	gpio_pin_set_dt(&led2, 0);
	gpio_pin_set_dt(&led1, 0);
    k_timer_start(&error_led_timer, K_MSEC(ERROR_INTERVAL), K_MSEC(ERROR_INTERVAL));
   
    gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_DISABLE);
    gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_DISABLE);
}

static void errorvbus_run(void *o){
    LOG_INF("in error vbus run");
	//k_msleep(1000);
	//k_timer_stop(&error_led_timer);
	usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
    if (usbregstatus == 0){
        smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
    }
}

static void errorvbus_exit(void *o){
    LOG_INF("in error vbus exit");
    //k_timer_stop(&error_led_timer);
	//k_timer_start(&led_timer, K_MSEC(LED_INTERVAL), K_MSEC(LED_INTERVAL)); 
    //smf_set_state(SMF_CTX(&s_obj), &my_states[measure]);
}

//Heartbeat handlers
void heartbeat_timer_handler(struct k_timer *heartbeat_timer){
	k_work_submit(&hb_timer_work);
}
void heartbeat_timer_work_handler(struct k_work *heartbeat_timer_work_handler){
	gpio_pin_toggle_dt(&heartbeat);
	LOG_INF("lub-dub");
}

// LEDs handlers
void led_timer_start_handler(struct k_timer *led_timer){
	k_work_submit(&led_timer_start_work);
}
void led_timer_stop_handler(struct k_timer *led_timer){
	k_work_submit(&led_timer_stop_work);
}
		float min_point13;
		float max_point13;
void led_timer_start_work_handler(struct k_work *led_timer_start_work){ 
    LOG_INF("hey! led timer workinggggg hereeee");

	//i = 0;
	//i2 = 0; 

	//for (int i = 0; i < ARR_SIZE; i++){
		
		//data_arr[i++] = val_mv1;
		//data_arr2[i2] = val_mv2;

		//i++;
		//i2++;
		

		if (i < ARR_SIZE) {
			read();
			data_arr[i] = val_mv;
			data_arr2[i++] = val_mv1;

			// if(sizeof(data_arr) < 1){
			// 	min_point13 = 0;
			// 	max_point13 = 0;
			// }
			// else{
			// 	if(data_arr[i] < min_point13){
			// 		min_point13 = data_arr[i];
			// 	}
			// 	if(max_point13 < data_arr[i]){
			// 		max_point13 = data_arr[i];
			// 	}
			// }
			// LED1_bright = max_point13 - min_point13;
			// LOG_INF("Whats the value : %d", data_arr[i]);
			// pwm_set_pulse_dt(&led_pwm, (int)((LED1_bright/10)*22222));
		}

		else if (i = ARR_SIZE){
			k_timer_stop(&led_timer);
			//smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
			LOG_INF("LED STOP Array i is stopped");
			for (int i = 0; i < ARR_SIZE; i++){
				LOG_INF("array_data_led for first array[%d] = %f", i, data_arr[i]);
				LOG_INF("array_data_led for second array[%d] = %f", i, data_arr2[i]);
			}
			int hel_max = 0;
			int hel_min = 0;

			int k = 0;
			for(int d = 0; d < 100; d++){
				if(d == 9 || d == 19 || d == 29 || d == 39 || d == 49 || d == 59 || d == 69 || d == 79 || d == 89 || d == 99){
					k++;
				}
					else{
						if(data_arr[d] > max_val[k]){
						max_val[k] = data_arr[d];
						}
						else if(data_arr[d] < min_val[k]){
						min_val[k] = data_arr[d];
						}
						if(hel_max < data_arr[d]){
							hel_max = data_arr[d];
						}
						if(hel_min > data_arr[d]){
							hel_min = data_arr[d];
						}



					}
			}
			k = 0;

				LOG_INF("array_data_led for first max [%d]", hel_max);
				LOG_INF("array_data_led for first min  [%d]", hel_min);
				LOG_INF("array_data_led for VPP  [%d]", (hel_max-hel_min)/100);
				//LOG_INF("array_data_led for first min  [%d]", hel_min);



		for(int d = 0; d < 100; d++){
				if(d == 9 || d == 19 || d == 29 || d == 39 || d == 49 || d == 59 || d == 69 || d == 79 || d == 89 || d == 99){
					
					k++;
				}
					else{
						if(data_arr2[d] > max_val2[k]){
						max_val2[k] = data_arr2[d];
						}
						else if((float)data_arr2[d] < 0.0f){
						min_val2[k] = data_arr2[d];
						}
					}
			}
		
	
		for(int v = 0; v < 10; v++){
			if(avg_max < max_val[v]){
				avg_max = max_val[v];
			}
			if(avg_min > min_val[v]){
				avg_min = min_val[v];
			}

			if(avg_max2 < max_val2[v]){
				avg_max2 = max_val2[v];
			}
			if(avg_min2 > min_val2[v]){
				avg_min2 = min_val2[v];
			}
		}

		// avg_max = avg_max/10;
		// avg_min = avg_min/10;

		// avg_max2 = avg_max2/10;
		// avg_min2 = avg_min2/10;

		LOG_INF("MAX AVERAGE: CHANNEL 1 %d", avg_max);
		LOG_INF("MIN AVERAGE: CHANNEL 1 %d", avg_min);

		LOG_INF("MAX AVERAGE: CHANNEL 2 %d", avg_max2);
		LOG_INF("MIN AVERAGE: CHANNEL 2 %d", avg_min2);
		}
		
		

		// if (i2 >= ARR_SIZE) {
		// 	k_timer_stop(&led_timer);
		// 	LOG_INF("LED STOP Array i2 is stopped");
		// }

		//smf_set_state(SMF_CTX(&s_obj), &my_states[measure]);
	//} //for loop bracket 
}

void led_timer_stop_work_handler(struct k_work *led_timer_stop_work){
	LOG_INF("hey! led timer work handler");
	//int64_t min_point1 = data_arr[0];
    //int64_t max_point1 = data_arr[0];
	float min_point1;
	float max_point1;
    //int64_t min_point2 = data_arr2[0];
    //int64_t max_point2 = data_arr2[0];

	// for (int i = 0; i < ARR_SIZE; i++){
	// 	LOG_INF("array_data[%d] = %f", i, data_arr[i]);
	// 	//smf_set_state(SMF_CTX(&s_obj), &my_states[measure]);
	// }


// I commented this out 1:59
    // for (int i = 0; i < ARR_SIZE; i++){

	// 	min_point1 = data_arr[i];
	// 	LOG_INF("min_point1: %f", min_point1);
    // 	max_point1 = data_arr[i];
	// 	LOG_INF("max_point1: %f", max_point1);

	// 	for (int j = 0; j < ARR_SIZE; j++) {
	// 		if (data_arr[j] < min_point1){
	// 			min_point1 = data_arr[j];
	// 			LOG_INF("Min Value: %f", min_point1);
	// 		}
	// 		if (data_arr[j] > max_point1){
	// 			max_point1 = data_arr[j];
	// 			LOG_INF("Min Value: %f", max_point1);
	// 		}

	// 		// if (data_arr2[i2] < min_point2){
	// 		// 	min_point2 = data_arr2[i2];
	// 		// }
	// 		// else if (data_arr2[i2] > max_point2){
	// 		// 	max_point2 = data_arr2[i2];
	// 		// }
	// 	}
    // }
	//LOG_INF("LED 1 Max value: %f", max_point1);
	//LOG_INF("LED 1 Min value: %f", min_point1);




    // LED1_bright = max_point1 - min_point1;
	// pwm_set_pulse_dt(&led_pwm, (int)((LED1_bright/10)*5.66));
    //LED2_bright = max_point2 - min_point2;
    // pwm_set_pulse_dt(&led_pwm, (int)(LED1_bright));
    //pwm_set_pulse_dt(&led_pwm2, (int)(LED2_bright));

    //float data_arr[3] = {battery, LED1_bright, LED2_bright};
    LOG_INF("mY EyEs! Led 1: %f", LED1_bright);
	//LOG_INF("mY EyEs! Led 2: %f", LED2_bright);
	// LOG_INF("Pressure: ");
    smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);

   //k_timer_start(&led_timer, K_MSEC(LED_INTERVAL), K_MSEC(LED_INTERVAL)); 
	//LOG_INF("LED OFF");
}

// void led2_timer_stop_work_handler(struct k_work *led2_timer_stop_work){
// 	LOG_INF("hey! led2 timer work handler");
// 	//int64_t min_point1 = data_arr[0];
//     //int64_t max_point1 = data_arr[0];
// 	float min_point2;
// 	float max_point2;
//     //int64_t min_point2 = data_arr2[0];
//     //int64_t max_point2 = data_arr2[0];

// 	// for (int i = 0; i < ARR_SIZE; i++){
// 	// 	LOG_INF("array_data[%d] = %f", i, data_arr[i]);
// 	// 	//smf_set_state(SMF_CTX(&s_obj), &my_states[measure]);
// 	// }



//     for (int i = 0; i < ARR_SIZE; i++){

// 		min_point2 = data_arr2[i];
// 		LOG_INF("min_point1: %f", min_point2);
//     	max_point2 = data_arr2[i];
// 		LOG_INF("max_point1: %f", max_point2);

// 		for (int j = 0; j < ARR_SIZE; j++) {
// 			if (data_arr2[j] < min_point2){
// 				min_point2 = data_arr2[j];
// 				LOG_INF("Min Value 2: %f", min_point2);
// 			}
// 			if (data_arr2[j] > max_point2){
// 				max_point2 = data_arr2[j];
// 				LOG_INF("Min Value 2: %f", max_point2);
// 			}

// 			// if (data_arr2[i2] < min_point2){
// 			// 	min_point2 = data_arr2[i2];
// 			// }
// 			// else if (data_arr2[i2] > max_point2){
// 			// 	max_point2 = data_arr2[i2];
// 			// }
// 		}
//     }
// 	LOG_INF("LED 2 Max value: %f", max_point2);
// 	LOG_INF("LED 2 Min value: %f", min_point2);




//     // LED1_bright = max_point1 - min_point1;
// 	// pwm_set_pulse_dt(&led_pwm, (int)((LED1_bright/10)*5.66));
//     //LED2_bright = max_point2 - min_point2;
//     // pwm_set_pulse_dt(&led_pwm, (int)(LED1_bright));
//     //pwm_set_pulse_dt(&led_pwm2, (int)(LED2_bright));

//     //float data_arr[3] = {battery, LED1_bright, LED2_bright};
//     LOG_INF("mY EyEs! Led 2: %f", LED2_bright);
// 	//LOG_INF("mY EyEs! Led 2: %f", LED2_bright);
// 	// LOG_INF("Pressure: ");
//     smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);

//    //k_timer_start(&led_timer, K_MSEC(LED_INTERVAL), K_MSEC(LED_INTERVAL)); 
// 	//LOG_INF("LED OFF");
// }

// void led2_timer_start_work_handler(struct k_work *led2_timer_start_work){ 
// 	LOG_INF("hey! led timer workinggggg hereeee");

// 	//i = 0;
// 	//i2 = 0; 

// 	//for (int i = 0; i < ARR_SIZE; i++){
		
// 		//data_arr[i++] = val_mv1;
// 		//data_arr2[i2] = val_mv2;

// 		//i++;
// 		//i2++;
		

// 		if (i < ARR_SIZE) {
// 			read();
// 			data_arr2[i++] = val_mv2;

// 			// if(sizeof(data_arr) < 1){
// 			// 	min_point13 = 0;
// 			// 	max_point13 = 0;
// 			// }
// 			// else{
// 			// 	if(data_arr[i] < min_point13){
// 			// 		min_point13 = data_arr[i];
// 			// 	}
// 			// 	if(max_point13 < data_arr[i]){
// 			// 		max_point13 = data_arr[i];
// 			// 	}
// 			// }
// 			// LED1_bright = max_point13 - min_point13;
// 			// LOG_INF("Whats the value : %d", data_arr[i]);
// 			// pwm_set_pulse_dt(&led_pwm, (int)((LED1_bright/10)*22222));
// 		}

// 		else if (i = ARR_SIZE){
// 			k_timer_stop(&led2_timer);
// 			//smf_set_state(SMF_CTX(&s_obj), &my_states[idle]);
// 			LOG_INF("LED STOP Array i is stopped");
// 			for (int i = 0; i < ARR_SIZE; i++){
// 				LOG_INF("array_data_led2[%d] = %f", i, data_arr2[i]);
// 			}
// 		}
		
// 		for(int d = 0; d < 10; d++){
// 					for(int j = 0; j < 10; j++){
// 						if(data_arr2[j*d] > max_val2[d]){
// 							max_val2[d] = data_arr2[j*d];
// 						}
// 						if(data_arr2[j*d] < min_val2[d]){
// 							min_val2[d] = data_arr2[j*d];
// 						}
// 					}
// 				}
		
	
// 		for(int v = 0; v < 10; v++){
// 			avg_max2 += max_val2[v];
// 			avg_min2 += min_val2[v];
// 		}

// 		avg_max2 = avg_max2/10;
// 		avg_min2 = avg_min2/10;

// 		LOG_INF("MAX AVERAGE: %d", avg_max2);
// 		LOG_INF("MIN AVERAGE: %d", avg_min2);

// 		// if (i2 >= ARR_SIZE) {
// 		// 	k_timer_stop(&led_timer);
// 		// 	LOG_INF("LED STOP Array i2 is stopped");
// 		// }

// 		//smf_set_state(SMF_CTX(&s_obj), &my_states[measure]);
// 	//} //for loop bracket 
// }

// // void led2_timer_start_handler(struct k_timer *led2_timer){
// // 	k_work_submit(&led2_timer_start_work);
// // }
// // void led2_timer_stop_handler(struct k_timer *led2_timer){
// // 	k_work_submit(&led2_timer_stop_work);
// // }


void error_led_timer_handler(struct k_timer *error_led_timer){
	k_work_submit(&error_led_timer_work);
}
void error_led_timer_work_handler(struct k_work *error_led_timer_work){
	gpio_pin_toggle_dt(&led3); 
	LOG_INF("uh oh");
}

// button 1 cb handlers
void button1_on_cb_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
	k_work_submit(&button1_on_cb_work);
}
void button1_on_cb_work_handler(struct k_work *button1_on_cb_work){
	k_timer_start(&led_timer, K_MSEC(LED_INTERVAL),K_MSEC(LED_INTERVAL));
	//k_timer_start(&led2_timer, K_MSEC(LED_INTERVAL),K_MSEC(LED_INTERVAL));
	LOG_INF("Someone pressed button 1 here");
	i = 0;
	smf_set_state(SMF_CTX(&s_obj), &my_states[measure]);
}

// button 2 cb handlers
void button2_on_cb_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins){
	k_work_submit(&button2_on_cb_work);
}
void button2_on_cb_work_handler(struct k_work *button2_on_cb_work){
	//smf_set_state(SMF_CTX(&s_obj), &my_states[blue]);
	LOG_INF("Someone pressed button 2 here");
}

/* Function Declarations Bluetooth*/
static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
void bluetooth_set_battery_level(int level, int nominal_batt_mv);
uint8_t bluetooth_get_battery_level(void);
int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length);
/* Function Declarations */

void on_notif_changed(enum bt_data_notifications_enabled status);
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
static struct bt_conn *current_conn;
void on_connected(struct bt_conn *conn, uint8_t ret);
void on_disconnected(struct bt_conn *conn, uint8_t reason);


/* BLE Callback Functions */
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

    notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

    if (remote_service_callbacks.notif_changed) {
        remote_service_callbacks.notif_changed(notifications_enabled);
    }
}

static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &bluetooth_output, sizeof(bluetooth_output));
}

static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
    LOG_INF("Received data, handle %d, conn %p", attr->handle, (void *)conn);
    
    if (remote_service_callbacks.data_rx) {
        remote_service_callbacks.data_rx(conn, buf, len);
    }
    return len;
}

void on_sent(struct bt_conn *conn, void *user_data) {
    ARG_UNUSED(user_data);
    LOG_INF("Notification sent on connection %p", (void *)conn);
}

void bt_ready(int ret) {
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
    }

    // release the thread once initialized
    k_sem_give(&bt_init_ok);
}

/* Setup BLE Services */
BT_GATT_SERVICE_DEFINE(remote_srv,
BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC,
                BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                BT_GATT_PERM_READ,
                read_data_cb, NULL, NULL),
BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_MESSAGE_CHRC,
                BT_GATT_CHRC_WRITE_WITHOUT_RESP,
                BT_GATT_PERM_WRITE,
                NULL, on_write, NULL),
);

int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length) {
    int ret = 0;

    struct bt_gatt_notify_params params = {0};
    const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

    params.attr = attr;
    params.data = &value;
    params.len = length;
    params.func = on_sent;

    ret = bt_gatt_notify_cb(conn, &params);

    return ret;
}

int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
    LOG_INF("Initializing Bluetooth");

    if ((bt_cb == NULL) | (remote_cb == NULL)) {
        return -NRFX_ERROR_NULL;
    }
    bt_conn_cb_register(bt_cb);
    remote_service_callbacks.notif_changed = remote_cb->notif_changed;
    remote_service_callbacks.data_rx = remote_cb->data_rx;

    int ret = bt_enable(bt_ready);
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
        return ret;
    }

 // hold the thread until Bluetooth is initialized
	k_sem_take(&bt_init_ok, K_FOREVER);

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		settings_load();
	}

	ret = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	if (ret) {
		LOG_ERR("Could not start advertising (ret = %d)", ret);
		return ret;
	}

	return ret;
}

/* Setup Callbacks */
struct bt_conn_cb bluetooth_callbacks = {
    .connected = on_connected,
    .disconnected = on_disconnected,
};
//THIS IS GIVING ME SO MANY ISSUESSSSS AND I DIDNT KNOW WHY IM SORRY
struct bt_remote_srv_cb remote_service_callbacks = {
    .notif_changed = on_notif_changed,
    .data_rx = on_data_rx,
	//on_notif_changed,
    //on_data_rx,
};

void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
    uint8_t temp_str[len+1];
    memcpy(temp_str, data, len);
    temp_str[len] = 0x00; // manually append NULL character at the end

    LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
    LOG_INF("Data: %s", temp_str);
}

void on_connected(struct bt_conn *conn, uint8_t ret) {
    if (ret) { LOG_ERR("Connection error: %d", ret); }
    LOG_INF("BT connected");
    current_conn = bt_conn_ref(conn);
}

void on_disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_INF("BT disconnected (reason: %d)", reason);
    if (current_conn) {
        bt_conn_unref(current_conn);
        current_conn = NULL;
    }
}

void on_notif_changed(enum bt_data_notifications_enabled status) {
    if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
        LOG_INF("BT notifications enabled");
    }
    else {
        LOG_INF("BT notifications disabled");
    }
}


float pwm_avg[2] = {};
int inc;

int r = 0;
float bright;
void read(void)
{
	
	LOG_INF("Measuring %s (channel %d)... ", adc_vadc.dev->name, adc_vadc.channel_id);
    LOG_INF("Measuring %s (channel %d)... ", adc_vadc1.dev->name, adc_vadc1.channel_id);
	//LOG_INF("Measuring %s (channel %d)... ", adc_vadc2.dev->name, adc_vadc2.channel_id);

		(void)adc_sequence_init_dt(&adc_vadc, &sequence);
		(void)adc_sequence_init_dt(&adc_vadc1, &sequence1);

		ret = adc_read(adc_vadc.dev, &sequence);
		if (ret < 0) {
			LOG_ERR("Could not read (%d)", ret);
		} else {
			LOG_DBG("Raw ADC Buffer: %d", buf);
		}

		int ret1;
		ret1 = adc_read(adc_vadc1.dev, &sequence1);
		if (ret1 < 0) {
			LOG_ERR("Could not read (%d)", ret1);
		} else {
			LOG_DBG("Raw ADC Buffer 1: %d", buf1);
		}

		// int ret2;
		// ret2 = adc_read(adc_vadc2.dev, &sequence2);
		// if (ret2 < 0) {
		// 	LOG_ERR("Could not read (%d)", ret2);
		// } else {
		// 	LOG_DBG("Raw ADC Buffer 1: %d", buf2);
		// }
		
		val_mv = buf;
		val_mv1 = buf1;
		val_mv2 = buf2;

		ret = adc_raw_to_millivolts_dt(&adc_vadc, &val_mv); // remember that the vadc struct containts all the DT parameters
		if (ret < 0) {
			LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
		} else {
			LOG_INF("ADC Value (mV): %d", val_mv);
		}
		ret1 = adc_raw_to_millivolts_dt(&adc_vadc1, &val_mv1); // remember that the vadc struct containts all the DT parameters
		if (ret1 < 0) {

			LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
		} else {
			
			
			pwm_set_pulse_dt(&led_pwm, led_pwm.period * 0);
			pwm_set_pulse_dt(&led_pwm2, led_pwm2.period * 0);
			
			
			
			LOG_INF("ADC Value 1 (mV): %d", val_mv1);
		}
		// ret2 = adc_raw_to_millivolts_dt(&adc_vadc2, &val_mv2); // remember that the vadc struct containts all the DT parameters
		// if (ret2 < 0) {
		// 	LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
		// } else {
		// 	LOG_INF("ADC Value 2 (mV): %d", val_mv2);
		// }
	//from channel1 
	//battery = ; //1000 mHz put into bluetooth battery thing
	//LOG_INF("Battery Level: %lld", battery);

 // 2:02
	// if(sizeof(data_arr) < 1){
	// 	min_point13 = 0;
	// 	max_point13 = 0;
	// }
	// else{
	// 	if(data_arr[i] < min_point13){
	// 		min_point13 = data_arr[i];
	// 	}
	// if(max_point13 < data_arr[i]){
	// 	max_point13 = data_arr[i];
	// }
	// }

	// if(inc < 3){
	// 	LED1_bright = max_point13 - min_point13;
	// 	LED1_bright = (LED1_bright/10);
	// 	pwm_avg[inc] = LED1_bright;
	// }
	// if(inc >= 3){
	// r = r + 0.1;
	// if(r == 1.1){
	// 	r = 0;
	// }
   

	// double bright = 0;
	// LOG_INF("Print Bright array is!!!! : %d", val_mv1);
	// if(val_mv1 > 0){
	// 	bright = val_mv1/100;
	// 	LOG_INF("Bright is greater than zero!!!! : %d", bright);
	// }
	// if(val_mv1 < 0){
	// 	bright = val_mv1 * 2;
	// 	LOG_INF("WBright is less than zero!!!! : %d", bright);
	// }
	
	// int hello;
	// hello = data_arr[i];
	// LOG_INF("This is the brightness!!!! : %d", bright);
	// //pwm_set_pulse_dt(&2pwm, led_pwm.period *(0.01) *bright);
	// inc = 0;
	// }
	// inc++;
}
// Main Loop
void main(void){

	int32_t ret;

	// set initial state
	smf_set_initial(SMF_CTX(&s_obj), &my_states[init]);


	// Run the state machine
    while(1) {
        // State machine terminates if a non-zero value is returned
        ret = smf_run_state(SMF_CTX(&s_obj));
		if (ret) {
			// handle return code and terminate state machine 
			break;
		}

        // usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
        // if (usbregstatus <= 0) {
        // // VBUS detected
       	// 	smf_set_state(SMF_CTX(&s_obj), &my_states[errorvbus]);
        // } 
        // else {
        //     // VBUS not detected
        //     //smf_set_state(SMF_CTX(&s_obj), &my_states[measure]);
        // }

		//k_msleep(1000);
	}
}

// BLUETOOTH STUFFFFFFF

// Bluetooth include statments
// #include <zephyr/bluetooth/bluetooth.h>
// #include <zephyr/bluetooth/uuid.h>
// #include <zephyr/bluetooth/gatt.h>
// #include <zephyr/bluetooth/hci.h> // host controller interface
// #include <zephyr/bluetooth/services/bas.h>  // battery service GATT
// #include <zephyr/settings/settings.h>

// Bluetooth defines
// /* Define macros for UUIDs of the Remote Service
//   Project ID: 001 (3rd entry)
//   MFG ID = 0x01FF (4th entry)
//*/
// configing button 2 
// #define BT_UUID_REMOTE_SERV_VAL \
//         BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
// #define BT_UUID_REMOTE_SERVICE 			BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

// /* UUID of the Data Characteristic */
// #define BT_UUID_REMOTE_DATA_CHRC_VAL \
//         BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
// #define BT_UUID_REMOTE_DATA_CHRC 		BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

// /* UUID of the Message Characteristic */
// #define BT_UUID_REMOTE_MESSAGE_CHRC_VAL \
//         BT_UUID_128_ENCODE(0xe9ea0002, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
// #define BT_UUID_REMOTE_MESSAGE_CHRC 	BT_UUID_DECLARE_128(BT_UUID_REMOTE_MESSAGE_CHRC_VAL) 

// #define DEVICE_NAME CONFIG_BT_DEVICE_NAME
// #define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
// #define BLE_DATA_POINTS 600 // limited by MTU

// enumeration to keep track of the state of the notifications
// enum bt_data_notifications_enabled {
//     BT_DATA_NOTIFICATIONS_ENABLED,
//     BT_DATA_NOTIFICATIONS_DISABLED,
// };
// enum bt_data_notifications_enabled notifications_enabled;

// declare struct of the BLE remove service callbacks
// struct bt_remote_srv_cb {
//     void (*notif_changed)(enum bt_data_notifications_enabled status);
//     void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
// }; 
// static struct bt_remote_srv_cb remote_service_callbacks;

// // blocking thread semaphore to wait for BLE to initialize
// static K_SEM_DEFINE(bt_init_ok, 1, 1);

// /* Advertising data */
// static const struct bt_data ad[] = {
//     BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
//     BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
// };

// /* Scan response data */
// static const struct bt_data sd[] = {
//     BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
// };

// /* Function Declarations */
// static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
// void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
// static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
// void bluetooth_set_battery_level(int level, int nominal_batt_mv);
// uint8_t bluetooth_get_battery_level(void);

// /* Setup BLE Services */
// BT_GATT_SERVICE_DEFINE(remote_srv,
// BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
// BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC,
//                 BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
//                 BT_GATT_PERM_READ,
//                 read_data_cb, NULL, NULL),
// BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
// BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_MESSAGE_CHRC,
//                 BT_GATT_CHRC_WRITE_WITHOUT_RESP,
//                 BT_GATT_PERM_WRITE,
//                 NULL, on_write, NULL),
// );

// /* BLE Callback Functions */
// void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
//     bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
//     LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

//     notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

//     if (remote_service_callbacks.notif_changed) {
//         remote_service_callbacks.notif_changed(notifications_enabled);
//     }
// }

// static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
//     return bt_gatt_attr_read(conn, attr, buf, len, offset, &compliance_data, sizeof(compliance_data));
// }

// static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
//     LOG_INF("Received data, handle %d, conn %p", attr->handle, (void *)conn);
    
//     if (remote_service_callbacks.data_rx) {
//         remote_service_callbacks.data_rx(conn, buf, len);
//     }
//     return len;
// }

// void on_sent(struct bt_conn *conn, void *user_data) {
//     ARG_UNUSED(user_data);
//     LOG_INF("Notification sent on connection %p", (void *)conn);
// }

// void bt_ready(int ret) {
//     if (ret) {
//         LOG_ERR("bt_enable returned %d", ret);
//     }

//     // release the thread once initialized
//     k_sem_give(&bt_init_ok);
// }

// int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length) {
//     int ret = 0;

//     struct bt_gatt_notify_params params = {0};
//     const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

//     params.attr = attr;
//     params.data = &value;
//     params.len = length;
//     params.func = on_sent;

//     ret = bt_gatt_notify_cb(conn, &params);

//     return ret;
// }

// /* Initialize the BLE Connection */
// int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
//     LOG_INF("Initializing Bluetooth");

//     if ((bt_cb == NULL) | (remote_cb == NULL)) {
//         return -NRFX_ERROR_NULL;
//     }
//     bt_conn_cb_register(bt_cb);
//     remote_service_callbacks.notif_changed = remote_cb->notif_changed;
//     remote_service_callbacks.data_rx = remote_cb->data_rx;

//     int ret = bt_enable(bt_ready);
//     if (ret) {
//         LOG_ERR("bt_enable returned %d", ret);
//         return ret;
//     }
// }

// /* Function Declarations */
// static struct bt_conn *current_conn;
// void on_connected(struct bt_conn *conn, uint8_t ret);
// void on_disconnected(struct bt_conn *conn, uint8_t reason);
// void on_notif_changed(enum bt_data_notifications_enabled status);
// void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);

// /* Setup Callbacks */
// struct bt_conn_cb bluetooth_callbacks = {
//     .connected = on_connected,
//     .disconnected = on_disconnected,
// };

// struct bt_remote_srv_cb remote_service_callbacks = {
//     .notif_changed = on_notif_changed,
//     .data_rx = on_data_rx,
// };

// void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
//     uint8_t temp_str[len+1];
//     memcpy(temp_str, data, len);
//     temp_str[len] = 0x00; // manually append NULL character at the end

//     LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
//     LOG_INF("Data: %s", temp_str);
// }

// void on_connected(struct bt_conn *conn, uint8_t ret) {
//     if (ret) { LOG_ERR("Connection error: %d", ret); }
//     LOG_INF("BT connected");
//     current_conn = bt_conn_ref(conn);
// }

// void on_disconnected(struct bt_conn *conn, uint8_t reason) {
//     LOG_INF("BT disconnected (reason: %d)", reason);
//     if (current_conn) {
//         bt_conn_unref(current_conn);
//         current_conn = NULL;
//     }
// }

// void on_notif_changed(enum bt_data_notifications_enabled status) {
//     if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
//         LOG_INF("BT notifications enabled");
//     }
//     else {
//         LOG_INF("BT notifications disabled");
//     }
// }

// void main(void) {
//     /* Initialize Bluetooth */
//     int err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
//     if (err) {
//         LOG_ERR("BT init failed (err = %d)", err);
//     }

//     // do stuff, write to "data" array

//     // send a notification that "data" is ready to be read...
//     err = send_data_notification(current_conn, data, 1);
//     if (err) {
//         LOG_ERR("Could not send BT notification (err: %d)", err);
//     }
//     else {
//         LOG_INF("BT data transmitted.");
//     }


//     /* Example of how to use the Battery Service
//        `normalized_level` comes from an ADC reading of the battery voltage
//        this function populates the BAS GATT with information
//     */
//     err = bt_bas_set_battery_level((int)normalized_level);
//     if (err) {
//         LOG_ERR("BAS set error (err = %d)", err);
//     }

//     // this function retrieves BAS GATT with information
//     battery_level =  bt_bas_get_battery_level();
// }